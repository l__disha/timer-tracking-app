import React from 'react';
import EditableTimerList from './EditableTimerList';
import ToggleableTimerForm from './ToggleableTimerForm';
import {v4 as uuidv4 } from 'uuid';
import Helpers from '../helpers';
import Client from '../client';

class TimersDashboard extends React.Component {

    state = {
        timers: []
    };

    componentDidMount() {
        this.loadTimersFromServer();
        setInterval(this.loadTimersFromServer, 5000);
    }

    loadTimersFromServer = () => {
        Client.getTimers((serverResponse) =>{ 
            this.setState({timers: serverResponse});
        })
    }

    handleCreateFormSubmit = (timer) => {
        this.createTimer(timer)
    }

    handleEditFormSubmit = (timer) => {
        this.updateTimer(timer);
    }

    handleTrashClick = (timerId) => {
        this.deleteTimer(timerId);
    }

    handleStartClick = (timerId) => {
        this.startTimer(timerId);
    }

    handleStopClick = (timerId) => {
        this.stopTimer(timerId);
    }

    createTimer = (timer) => {
        const t = Helpers.newTimer(timer);
        this.setState({timers: this.state.timers.concat(t) });

        //Optimized Update: updating the states on client side without informing/conversing with sever
        Client.createTimer(t);
    }

    updateTimer = (updateedTimer) => {
        this.setState ( {
            timers: this.state.timers.map((timer) => {
                if(timer.id === updateedTimer.id) {
                    return Object.assign( {}, timer, {
                        title: updateedTimer.title,
                        project: updateedTimer.project
                    });
                }
                return timer;
            })
        });

        Client.updateTimer(updateedTimer);
    }

    deleteTimer = (timerId) => {
        this.setState( {
            timers: this.state.timers.filter(timer => timer.id !== timerId),
        });

        Client.deleteTimer({ id: timerId});
    }

    startTimer = (timerId) => {
        const now = Date.now();

        this.setState({
            timers: this.state.timers.map((timer) => {
                if(timer.id === timerId) {
                    return Object.assign({}, timer, {
                        runningSince:now
                    });
                }
                return timer;
            })
        });

        Client.startTimer({ id: timerId, start: now });
    }

    stopTimer = (timerId) => {
        const now = Date.now();

        this.setState({
            timers: this.state.timers.map(timer => {
                if(timer.id === timerId) {
                    const lastElapsed = now -timer.runningSince;

                    return Object.assign({}, timer, {
                        elapsed: timer.elapsed + lastElapsed,
                        runningSince: null
                    })
                }
                return timer;
            })
        });

        Client.stopTimer({ id: timerId, stop: now });
    }
    render() {
        return (
            <div className='ui three column centered grid'>
                <div className='column'>
                    <EditableTimerList 
                        timers={this.state.timers}
                        onFormSubmit={this.handleEditFormSubmit}
                        onTrashClick={ this.handleTrashClick }
                        onStartClick={this.handleStartClick}
                        onStopClick={ this.handleStopClick}
                    />
                    <ToggleableTimerForm 
                        onFormSubmit={this.handleCreateFormSubmit}
                    />
                </div>
            </div>
        )
    }
}

export default TimersDashboard;